FROM ubuntu:latest

MAINTAINER Chris Pergrossi <c.pergrossi@ufl.edu>

RUN apt-get update && apt-get upgrade -y && apt-get install -y openssh-server \
            git vim

RUN useradd -U -m spim

RUN mkdir -p /home/spim/bin

WORKDIR /home/spim/

RUN echo "spim:spim" | chpasswd && echo "spim ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/spim

RUN mkdir /home/spim/projects && chown spim:spim /home/spim/projects \
    && chmod 770 /home/spim/projects

ADD spim /home/spim/bin/spim
ADD exceptions.s /home/spim/exceptions.s

RUN chown spim:spim exceptions.s && chmod 660 exceptions.s && \
    echo "alias spim='spim -exception_file /home/spim/exceptions.s'" >> /home/spim/.profile

ADD run.sh /home/spim/run.sh
RUN chmod +x /home/spim/run.sh

ADD upload /usr/bin/upload
RUN chmod +x /usr/bin/upload

RUN mkdir /projects

WORKDIR /projects

VOLUME ['/projects']

CMD /home/spim/run.sh
