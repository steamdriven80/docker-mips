#!/bin/bash

shift

cd /projects

for FILE in /projects/*.s; do
  /home/spim/bin/spim -exception_file /home/spim/exceptions.s -file $FILE $@
done

if [ "$FILE" == "" ]; then
  /home/spim/bin/spim -exception_file /home/spim/exceptions.s
fi
